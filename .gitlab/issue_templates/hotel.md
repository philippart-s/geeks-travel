🔗 \<site web url>

💶 \<cost 💲 not expensive / 💲💲 a little expensive / 💲💲💲 expensive>

☕ \<is there breakfast or not ?> :heavy_check_mark: :x: / 🍽️ <is there dinner or not ?> :heavy_check_mark: :x: 

📍 [adress with google map link](https://goo.gl/maps/xxxx)

🚅 : \<distance to the gare station>

🔇  / 🔊 \<is it quiet or not ?>
