Le but de ce repository est de lister les bons plans restaurants / hôtels lorsque l'on arrive dans une ville lors d'un déplacement (une conférence, une intervention, ...).

L'idée est de se focaliser plutôt sur ajouter un endroit qui est bien plutôt que de lister les endroits qui ne sont pas biens 😅. Et l'objectif n'est pas de concurrencer Trip Advisor !

L'ensemble est géré par des [issues](https://gitlab.com/philippart-s/geeks-travel/-/issues) ou un [Kanban](https://gitlab.com/philippart-s/geeks-travel/-/boards) qui permet d'avoir une vue rapide de la liste des restaurants, des hôtels ou de ce qui existe pour une ville donnée.

Il existe deux [templates](https://gitlab.com/philippart-s/geeks-travel/-/tree/main/.gitlab/issue_templates) lors de la création d'une issue permettant de créer une entrée pour un [restaurant](https://gitlab.com/philippart-s/geeks-travel/-/blob/main/.gitlab/issue_templates/restaurant.md) ou pour un [hôtel](https://gitlab.com/philippart-s/geeks-travel/-/blob/main/.gitlab/issue_templates/hotel.md).

Les [labels](https://gitlab.com/philippart-s/geeks-travel/-/labels) permettent de typer les issues : [restaurant](https://gitlab.com/philippart-s/geeks-travel/-/issues?label_name%5B%5D=%F0%9F%8D%BD%EF%B8%8F+Restaurant) ou [hôtel](https://gitlab.com/philippart-s/geeks-travel/-/issues?label_name%5B%5D=%F0%9F%8F%A8+H%C3%B4tel) mais aussi d'indiquer la ville.

La zone commentaires de l'issue permet d'ajouter des informations subsidiaires.
